import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import {Value} from './App';

const useStyles = makeStyles({
  root: {
    width: 200,
  },
});

/*
Component by function for rendering
*/
export default function ContinuousSlider() {
  const classes = useStyles();

  /*
  This notation is the equivalent of state in Component by class --> HOOKS
  useState make [value, setValue] stateful and it is set to the initial state
  of the variable value. It specifies that the methods setValue will update the 
  variable value
  */
  const [value, setValue] = React.useState(0);

  // Function to detect the change of the slider
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Typography id="continuous-slider" gutterBottom />
      <Grid container spacing={2}>
        <Grid item xs>
          <Slider value={value} onChange={handleChange} aria-labelledby="continuous-slider" />
        </Grid>
      </Grid>

      <div>
        <Value dataFromParent = {value} />
      </div>
    </div>
  );
}

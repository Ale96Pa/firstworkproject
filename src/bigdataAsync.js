/*
In this file there is the management and use of Elastic API for node.js
ASYNC VERSION: with Promises
*/
'use strict'

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })

// Query to delete an index and clean the dataset
async function deleteIfExist(){
  const { body } = await client.indices.exists({
    index: "regions"
  });

  if(body){
    const {body} = await client.indices.delete({
      index: 'regions'
    })
  }
}

// Query to insert data into an index
async function insertRegions(){

  // Get dataset from URL
  var request = require("request");
  request({uri: "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni-20201116.csv"}, 
    async function(error, response, body) {
      // Pars the csv file and store data in variables
      var rows = body.split("\n");
      for(var i=1; i<rows.length-1; i++){
        var data = rows[i].split(",");
        var name = data[3];
        var latitude = String(data[4]);
        var longitude = String(data[5]);
        var totPositive = String(data[17]);
        var tampons = String(data[18]);

        var ricoverati_con_sintomi = String(data[6]);
        var terapia_intensiva = String(data[7]);
        var totale_ospedalizzati = String(data[8]);
        var isolamento_domiciliare = String(data[9]);
        var totale_positivi = String(data[10]);

        // Insert data in index row by row
        const {body} = await client.index({
          index: 'regions',
          body: {
              name: name,
              latitude: latitude,
              longitude: longitude,
              case: totPositive,
              tamp: tampons,
              ricoverati_con_sintomi: ricoverati_con_sintomi,
              terapia_intensiva: terapia_intensiva,
              totale_ospedalizzati: totale_ospedalizzati,
              isolamento_domiciliare: isolamento_domiciliare,
              totale_positivi: totale_positivi
          }
        });
        console.log(body.result);
      }   
  });
}

/*
Function to return the regions with a latitude grater or equal a certain value
passed as parameter
*/
exports.getRegions = async function getRegions(latValue){
  await client.indices.refresh({ index: 'regions' })

  const {body} = await client.search({
    index: 'regions',
    size: 25,
    body: {
      query: {
        range: {
          latitude: {
            gte: latValue
          }
        }
      }
    }
  });

  var arrayReg = body.hits.hits;
  var regs = [];
  arrayReg.forEach(element => {
      regs.push(element._source);
  });
  return regs;
}

/*
Function to delete and insert the data. It also has a test of search query.
Executed once.
*/
async function run() {
  deleteIfExist();
  insertRegions();
  /*
  await client.indices.refresh({ index: 'regions' })
  const regs = await getRegions();
  return regs;
  */
}

//run();

/*
The Server receives the parameters from React and query Elastic. Then return the results
of the query to React.
*/
const elast = require('./bigdataAsync');
const express = require("express");
const bodyParser = require("body-parser");

const app = express();

// Parse requests of content-type - application/json
app.use(bodyParser.json());
// Parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

/*
Method post and get for web api.
  -) app.post receive the value of the slider
  -) app.get send the query results 
*/
app.route('/')
  .post(function (req, res){
    const vals = req.body.v;
    res.send("ok"); // Response to close the comunication
    // Set a param 'val' that is red in app.get to pass the value of the slider as param
    app.set('val', vals); 
  })

  .get(function(req, res){
    const latValue = app.get('val');
    //console.log(latValue);

    // Query to Elastic: it is a Promise
    elast.getRegions(parseFloat(latValue)).then(
      function (value){
        res.json({ message: value });
      },
      function (err){
        console.log("error");
      }
    )
  });  

// Set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
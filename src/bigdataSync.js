/*
In this file there is the management and use of Elastic API for node.js
SYNC VERSION: with callback
NOT USED --> difficulty in importing to the server
*/
'use strict'

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })

function deleteIfExist(){
    client.indices.exists({
        index: 'regions',
    }, (err, result) => {
        if (err) console.log(err);
        //console.log(result.body);
        const isRegionAlive = result.body;

        if(isRegionAlive){
            client.indices.delete({
                index: 'regions',
            }, (err, result) => {
                //console.log(result.body);
            })
        }
    })
}

function insertRegions(){
    var regions=[];
    var request = require("request");
    request({uri: "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni-20201116.csv"}, 
        function(error, response, body) {
            var rows = body.split("\n");
            for(var i=1; i<rows.length-1; i++){
                var data = rows[i].split(",");
                var name = data[3];
                var latitude = String(data[4]);
                var longitude = String(data[5]);
                var totPositive = String(data[17]);
                
                client.index({
                    index: 'regions',
                    body: {
                        name: name,
                        latitude: latitude,
                        longitude: longitude,
                        cases: totPositive
                    }
                }, (err, result) => {
                    //console.log(result.body.result);
                });
            }
        }
    );
}

function getRegions(prop){
    client.search({
        index: 'regions'
    }, (err, response) =>{
        var arrayReg = response.body.hits.hits;
        var regs = [];
        arrayReg.forEach(element => {
           regs.push(element._source);
        });
        return regs;
    })
    //console.log(response);
}

exports.elasticRegions = function run (prop) {
    //deleteIfExist();    
    //insertRegions();
    getRegions(prop);
    //console.log(prop);
}

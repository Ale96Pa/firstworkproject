import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Header} from './Presentation'
import ContinuousSlider from './Slider';
import {Radar} from './Radar'
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <Header />
  </React.StrictMode>,
  document.getElementById('headerPage')
);
/* Graph is renderd from Value component
ReactDOM.render(
  <React.StrictMode>
    <Graph />
  </React.StrictMode>,
  document.getElementById('root')
);
*/
ReactDOM.render(
  <React.StrictMode>
    <ContinuousSlider/>
  </React.StrictMode>,
  document.getElementById('slider')
);

ReactDOM.render(
  <React.StrictMode>
    <Radar/>
  </React.StrictMode>,
  document.getElementById('radar')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

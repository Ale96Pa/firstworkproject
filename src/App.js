import React, { Component } from 'react';
import './App.css';
import 'react-vis/dist/style.css';
import axios from 'axios';
import {XYPlot, VerticalGridLines, HorizontalGridLines, XAxis, YAxis, MarkSeries, Hint, ParallelCoordinates} from 'react-vis';

// Class for plotting the main graphs: Italy and Parallel Coordinate
class Graph extends Component{

  constructor(props) {
    super(props);

    this.state = {
        hintData : {},
        hintHover : false,
        apiResponse: [],
        regions: [],
        regionsNames: [],
        lat: 0
    };
    this.getHintSection = this.getHintSection.bind(this);
    this.mouseOver = this.mouseOver.bind(this);
    this.mouseOut = this.mouseOut.bind(this);
    this.callAPI = this.callAPI.bind(this);
  }

  /*
  Retrieve information from localhost:8080 to get the query returned by Elastic
  */
  callAPI() {
    axios.get('http://localhost:8080')
      // Handle success (response is in res)
      .then((res) => {
        const regs = res.data.message;
        this.setState({ apiResponse: regs });
        var auxRegions = [];
        var auxRegionsNames = [];
        for(var i=0; i<regs.length; i++){
          auxRegions.push({x: parseFloat(regs[i].longitude), y: parseFloat(regs[i].latitude), size: parseFloat(regs[i].case)});
          auxRegionsNames.push({name: regs[i].name, x: parseFloat(regs[i].longitude), y: parseFloat(regs[i].latitude), cases: parseFloat(regs[i].case), tamps: parseFloat(regs[i].tamp)});
        }
        this.setState({ regions : auxRegions });
        this.setState({ regionsNames : auxRegionsNames });
      })
      // Handle error
      .catch((error) => {
        console.log(error);
    });
  }

  /*
  First call to query to set the first retrieved data
  */
  componentDidMount() {
    this.callAPI();
  }

  /*
  Function to call the Hint component to display regions' names
  Question mark operator ==> condition ? ops if true : ops if false
  */
  getHintSection(isHintVisible) {
      return isHintVisible ?
      <Hint value={this.state.hintData}>
          <div style={{background: 'black'}}>
                {this.state.hintData.n}
          </div>
      </Hint> : null;
  }

  /*
  Functions to handle the event when mouse is over / out
  */
  mouseOver(datapoint) {
    for(var i=0; i<this.state.regionsNames.length; i++){
      if(this.state.regionsNames[i].x === datapoint.x & this.state.regionsNames[i].y === datapoint.y){
        this.setState({hintData : {x: datapoint.x, y: datapoint.y, n: this.state.regionsNames[i].name}, hintHover :true});
      }
    }
  }
  
  mouseOut(datapoint) {
    this.setState({hintData : datapoint, hintHover :false});
  }

  /*
  Function to detect when componen has changed: prevProps and prevState access to 
  values before the change. The operations are executed AFTER the changes.
  */
  componentDidUpdate(prevProps, prevState) {
    if(this.state.lat !== prevProps.dataFromParent){
      this.callAPI();
    }
  }

  /*
  Function to detect when componen has changed: nextProps accesses the values
  after the change. The operations are executed BEFORE the changes.
  */
  componentWillReceiveProps(nextProps) {
    if (this.props.dataFromParent !== nextProps.dataFromParent) {
      this.setState({
        lat: nextProps.dataFromParent
      });
      //console.log(this.state.lat);
    }
  }

  render(){
    return (
    <div className="containerGraphs">
      
      <div className="borderItaly">
        <img src="https://banner2.cleanpng.com/20180403/lgw/kisspng-regions-of-italy-vicenza-vector-map-italy-5ac44c10f3d217.7499415115228139689987.jpg" 
        alt="border It" width="400px" height="400px" />
      </div>

      <div className="plot">
        <XYPlot
          width={300}
          height={400}>

          <HorizontalGridLines />
          <VerticalGridLines />
          <YAxis title="Y" />
          <XAxis title="X" />
          
          <MarkSeries
            color="red"
            data={this.state.regions}
            onValueMouseOver={this.mouseOver}
            onValueMouseOut={this.mouseOut} />

          {// Render the Hint component: necessary to render condionally
          this.getHintSection(this.state.hintHover)}

        </XYPlot>
      </div>

      <div className="image">
        <img className="img" src="https://databenc.it/newsite/wp-content/uploads/2019/12/cini-300x200-1.jpg" 
        alt="small logo" width="200px" />
      </div>

      <div className="parallel">
        <ParallelCoordinates 
          height = {250}
          width = {400}
          data={this.state.regionsNames} 
          domains={[
            {name: 'x', domain: [0, 100]},
            {name: 'y', domain: [0, 100]},
            {name: 'cases', domain: [0, 400000]}
          ]}
          brushing={true}
          style={{
            labels: {
              textAnchor: 'right',
              y: "-10"
            }
          }}
        />
      </div>
    </div>
    );
  }
}


// Class to manage the retrieve of data from the slider. It passes data to Graph
class Value extends Component{
  constructor(props){
    super(props);

    this.state = {
        mappedValue : 0
    };
  }

  /*
  This function connect to localhost:8080 to send the value of the slider (that will be used
  to query Elastic)
  */
  sendQuery(){
    const val = {
      v: this.state.mappedValue
    };

    axios.post("http://localhost:8080/", val)
      // Handle success (response get after the post is in res)
      .then( res => {
        })
      // Handle failure of connection
      .catch(() => {
            console.log("Something went wrong. Plase try again later");
        });
  }

  componentDidUpdate(prevProps){
    if (this.props.dataFromParent !== prevProps.dataFromParent) {
      this.setState({
        mappedValue: (parseInt(this.props.dataFromParent)*46/100)
      });
    }
  }

 render() {
  // Each time the slider is rendered, a new query must be triggered
  this.sendQuery();
  return (
    <div>
      <div>
          Altitude is: {this.state.mappedValue}
      </div>

      <div>
        <Graph dataFromParent = {this.state.mappedValue} />
      </div>
    </div>
  );
  }
}

export {Graph};
export {Value};